﻿using System;

namespace Mic.Lessons.Tables
{
    class Program
    {
        static void Main(string[] args)
        {
            uint tableNum = 1;
            while (true)
            {
                byte columns = 2;
                byte rows = 2;

                Console.WriteLine("Enter \"Exit\" for close the app. \nPlease enter number of columns (default = 2).");
                string readLine1 = Console.ReadLine();
                if (readLine1 == "Exit")
                    return;
                byte.TryParse(readLine1, out columns);
                Console.WriteLine("Enter \"Exit\" for close the app. \nPlease enter number of rows (default = 2).");
                string readLine2 = Console.ReadLine();
                if (readLine2 == "Exit")
                    return;
                byte.TryParse(readLine2, out rows);

                Table table = new Table
                {
                    Column = columns,
                    Row = rows
                };

                table.Draw();
                table.PrintHeader(tableNum);
                tableNum++;
            }
        }
    }

    
}
