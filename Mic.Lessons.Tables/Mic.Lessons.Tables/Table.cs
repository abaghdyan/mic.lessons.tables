﻿using System;

namespace Mic.Lessons.Tables
{
    class Table
    {
        public static int allowedSize = Console.BufferWidth / 7;
        public Table()
        {
            _row = 2;
            _column = 2;
        }
        private byte _row;
        public byte Row
        {
            get { return _row; }
            set
            {
                if (value != 0)
                    _row = value;
            }
        }
        private byte _column;
        public byte Column
        {
            get { return _column; }
            set
            {
                if (value != 0 && value <= allowedSize)
                    _column = value;
            }
        }

        public void Draw()
        {
            for (int row = 0; row < Row; row++)
            {
                for (int col = 0; col < Column; col++)
                    Console.Write(" " + new string('_', 5) + " ");
                Console.WriteLine();
                for (int height = 0; height < 2; height++)
                {
                    for (int col = 0; col < Column; col++)
                    {
                        Console.Write("|     |");
                    }
                    Console.WriteLine();
                }
                for (int col = 0; col < Column; col++)
                    Console.Write("|" + new string('_', 5) + "|");
                Console.WriteLine();
            }
        }

        public void PrintHeader(uint tableNum)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string headline = $"Table № {tableNum}.";
            Console.WriteLine(new string('*', headline.Length + 2));
            Console.WriteLine("*" + headline + "*");
            Console.WriteLine(new string('*', headline.Length + 2));
            Console.ResetColor();
        }
    }
}
